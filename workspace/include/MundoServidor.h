// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
//Autor Miguel  JImenez Sarria
#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Disparo.h"
#include "Socket.h"
#include "DatosMemCompartida.h"

class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();
	Esfera esfera;
	std::vector<Disparo*> ListaDisparos;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	DatosMemCompartida datosmemoria;
	DatosMemCompartida *datosmemoriapuntero;

	int puntos1;
	int puntos2;

	int fd; //descriptor de fichero
	int fifo_servidor_cliente;
	int fifo_cliente_servidor;

	pthread_t thid1;
	//pthread_attr_t atrib;


	Socket conexion;
	Socket comunicacion;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
