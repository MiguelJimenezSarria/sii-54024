#include"DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include "Vector2D.h"
#include "Raqueta.h"
float posraqueta;

int main(){
	int fd;
	DatosMemCompartida *datosmemoriapuntero;
	//abrimos el fichero solo escritura
		fd=open("datoscompartidos",O_RDWR);
			if (fd < 0) {
				perror("Error fichero memoria");
			}
		if ((datosmemoriapuntero=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0)))==MAP_FAILED){
					perror("Error mmap");
					close(fd);
					return(1);
		}
		close(fd);

while(1){

	posraqueta=(datosmemoriapuntero->raqueta1.y1+datosmemoriapuntero->raqueta1.y2)/2;

	if(datosmemoriapuntero->esfera.centro.y < posraqueta){
		datosmemoriapuntero->accion=-1; //abajo
	}
	else if(datosmemoriapuntero->esfera.centro.y==posraqueta){
		datosmemoriapuntero->accion=0;//nada
	}

	else if(datosmemoriapuntero->esfera.centro.y > posraqueta){
		datosmemoriapuntero->accion=1;
	}	//arriba
	usleep(25000);
}

	unlink("datoscompartidos");//desproyectamos
	munmap(datosmemoriapuntero,sizeof(DatosMemCompartida));

return 1;

}
