// MundoCliente.cpp: implementation of the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////

#include <fstream>
#include <sys/mman.h>
#include "MundoCliente.h"
#include "glut.h"
#include "Disparo.h"
#include <pthread.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	//close(fd);
	munmap(datosmemoriapuntero, sizeof(datosmemoria));
	unlink("datoscompartidos");
	/*close(fifo_servidor_cliente);
	unlink("FIFO_SERVIDOR_CLIENTE");

	close(fifo_cliente_servidor);
	unlink("FIFO_CLIENTE_SERVIDOR");
	*/

}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	for(int i=0; i<ListaDisparos.size(); i++){
			ListaDisparos[i]->Dibuja();
		}

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	

	char cad[200];
	//read(fifo_servidor_cliente,cad,sizeof(cad));
	//Socket
	comunicacion.Receive(cad, sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &esfera.radio, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);


	//datos compartidos
	//Actualizamos los datos

	datosmemoriapuntero->esfera=esfera;
	datosmemoriapuntero->raqueta1=jugador1;
	//ACciones del bot
	if(datosmemoriapuntero->accion==-1){
		OnKeyboardDown('s', 0, 0);
	}

	else if(datosmemoriapuntero->accion==0)
	{}

	else if(datosmemoriapuntero->accion==1){
		OnKeyboardDown('w', 0, 0);
	}

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char aux=key;
	switch(key)
	{
	//case 'a':jugador1.velocidad.x=-1;break;
	//case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;

	break;
	case 'w':jugador1.velocidad.y=4;

	break;
	case 'l':jugador2.velocidad.y=-4;

	break;
	case 'o':jugador2.velocidad.y=4;

	break;
	case 'd': ListaDisparos.push_back(new Disparo(1,jugador1.GetPos()));break;
	case 'k': ListaDisparos.push_back(new Disparo(-1,jugador2.GetPos()));break;

	/*	case 's':sprintf(aux,"s");break;
		case 'w':printf(aux,"w");break;
		case 'l':printf(aux,"l");break;
		case 'o':printf(aux,"o");break;
*/
	}


	//Enviamos la tecla

	comunicacion.Send(&aux, sizeof(aux));
	//write(fifo_cliente_servidor,&key,sizeof(key));
}

void CMundoCliente::Init()
{

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


	/*

		//FIFO servidor-cliente
		mkfifo("FIFO_SERVIDOR_CLIENTE",0766);
		fifo_servidor_cliente=open("FIFO_SERVIDOR_CLIENTE",O_RDONLY);
		if(fifo_servidor_cliente<0){//error
			perror("Error servidor cliente");
			printf("Error servidor cliente");
			exit(EXIT_FAILURE);
			}

		//FIFO cliente-servidor
					mkfifo("FIFO_CLIENTE_SERVIDOR",0766);
					fifo_cliente_servidor=open("FIFO_CLIENTE_SERVIDOR",O_WRONLY);
					if(fifo_cliente_servidor<0){//error
							perror("Error cliente servidor");
							printf("Error cliente servidor");
							exit(EXIT_FAILURE);

							}

	*/

	//Conexion

	char ip[]="127.0.0.1"; //direccion ip
	char nombre[100]="miguel";
	printf("Introducir Nombre Jugador:\n");
	scanf("%s", nombre);
	comunicacion.Connect(ip,8001); //Puerto


	//Enviamos la informacion al servidor
	comunicacion.Send(nombre, sizeof(nombre));


		//abrir fichero para proyeccion en memoria de mmap

		int fdmmap;
		fdmmap=open("datoscompartidos",O_CREAT|O_TRUNC|O_RDWR,0766);
		if(fdmmap<0){
			//error
			 perror("Error fichero mmap");
			 return;

		}
		datosmemoria.esfera=esfera;
		datosmemoria.raqueta1=jugador1;
		datosmemoria.accion=0;
		//escribimos
		write(fdmmap,&datosmemoria,sizeof(datosmemoria));
		datosmemoriapuntero=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(datosmemoria), PROT_READ|PROT_WRITE, MAP_SHARED, fdmmap, 0));

		close(fdmmap);



}
