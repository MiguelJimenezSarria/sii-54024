// MundoServidor.cpp: implementation of the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include <sys/mman.h>
#include "MundoServidor.h"
#include "glut.h"
#include "Disparo.h"
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(fd);
	unlink("LOGGERTUBERIA");
	//close(fifo_servidor_cliente);
	////close(fifo_cliente_servidor);
}
void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{

	/*fifo_cliente_servidor=open("FIFO_CLIENTE_SERVIDOR",O_RDONLY);
	if(fifo_cliente_servidor<0){
		//error
		perror("error cliente servidor");
		return;
	}
	*/
     while (1) {
            usleep(10);
            char cad[100];
            //read(fifo_cliente_servidor, cad, sizeof(cad)); //Leemos de la tuberia
            comunicacion.Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}
void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	for(int i=0; i<ListaDisparos.size(); i++){
			ListaDisparos[i]->Dibuja();
		}

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	


	char cadena[200];
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	for(int i=0; i<ListaDisparos.size(); i++){
		ListaDisparos[i]->Mueve(0.025f);
	}
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	if(jugador1.Rebota(esfera)) //Si choca con raqueta reducimos tamaño
		esfera.ReducirTam();
	if(jugador2.Rebota(esfera)) //SI choca con raqueta reducimos tamaño
		esfera.ReducirTam();



	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++; //borrar luego
		esfera.SetRadio(0.5f);

		sprintf(cadena,"Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntos2);
		write(fd, &cadena, sizeof(cadena)); //escribimos por fifo
	}
	for(int i=0;i<ListaDisparos.size();i++){
		if(ListaDisparos[i]->centro.x<-7 || ListaDisparos[i]->centro.x>7){
			ListaDisparos.erase(ListaDisparos.begin()+i);}

		if(jugador2.Rebota(*ListaDisparos[i])){ //SI choca con raqueta eliminamos
			ListaDisparos.erase(ListaDisparos.begin()+i);
			jugador2.Reducir();
			jugador1.Agrandar();
		}

		if(jugador1.Rebota(*ListaDisparos[i])){ //SI choca con raqueta eliminamos
					ListaDisparos.erase(ListaDisparos.begin()+i);
					jugador1.Reducir();
					jugador2.Agrandar();
				}
	}


	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		esfera.SetRadio(0.5f);

		sprintf(cadena,"Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntos1);
		write(fd, &cadena, sizeof(cadena)); //escribimos por fifo logger
	}


	//Enviamos información al cliente

	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y,esfera.radio, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//write(fifo_servidor_cliente,cad,sizeof(cad));
	comunicacion.Send(cad,sizeof(cad));
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	//case 'a':jugador1.velocidad.x=-1;break;
	//case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'd': ListaDisparos.push_back(new Disparo(1,jugador1.GetPos()));break;
	case 'k': ListaDisparos.push_back(new Disparo(-1,jugador2.GetPos()));break;
	//Disparos no
	}
}

void CMundoServidor::Init()
{

	//Thread

	//pthread_create(&thid1, NULL, hilo_comandos, this);

	//Conectamos
		char ip[]="127.0.0.1";
		if(conexion.InitServer(ip,8001)==-1){
			//errror

		}
		comunicacion=conexion.Accept();
		char nombre[100];
		comunicacion.Receive(nombre, sizeof(nombre));
		printf("Se ha conectado a la partida el jugador %s \n",nombre);



		if((fd=open("LOGGERTUBERIA", O_WRONLY))<0){
				//error
				//perror("open");
				//return;
			}
		//Abrimos la tuberia servidor-cliente
		/*	fifo_servidor_cliente=open("FIFO_SERVIDOR_CLIENTE",O_WRONLY);
			if(fifo_servidor_cliente<0){//error}
				perror("open servidor cliente");
				return;
			}
			*/
			//Thread cliente servidor
			pthread_create(&thid1,NULL,hilo_comandos, this);



	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;






}
