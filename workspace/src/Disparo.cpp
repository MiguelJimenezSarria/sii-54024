// Disparo.cpp: implementation of the Disparo class.
//Miguel Jimenez Sarria
//////////////////////////////////////////////////////////////////////

#include "Disparo.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Disparo::Disparo()
{
	radio=0.1f;
	velocidad.x=0;
	velocidad.y=0;
}

Disparo::Disparo(int d, Vector2D pos)
{
	radio=0.1f;
	velocidad.x=5*d;
	velocidad.y=0;
	centro.x=pos.x;
	centro.y=pos.y;
}

Disparo::~Disparo()
{

}



void Disparo::Dibuja()
{
	glColor3ub(0,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Disparo::Mueve(float t)
{
	centro=centro+velocidad*t;
}

